//canvas
var canvas = document.getElementById('canvas') as HTMLCanvasElement
var ctx = canvas.getContext('2d')
// selecting new and fresh colours every time u reload the page
let randTriad: number = Math.floor(Math.random() * 3)
let randColour: number = Math.floor((Math.random() * 255))

//initialising the noise matrix + coordinates
let imax: number = canvas.width
let jmax: number = canvas.height
var noiseMatrix = []
for (var i = 0; i < imax; i++) {
    noiseMatrix[i] = []
    for (var j = 0; j < jmax; j++) {
        noiseMatrix[i][j] = 0;
    }
}

//mouse click nonsense and other unmentionable mutables:
var xpos: number = 0
var ypos: number = 0
var mouseDownTimeStamp: number = 0
var mousedown: boolean = false
var drawing: boolean = false

function setNotDrawing() {
    drawing = false
}

// draw the initial gradient
function draw() {
    for (var i = 0; i < imax; i++) {
        for (var j = 0; j < jmax; j++) {
            let iColour = Math.floor(255 - (255 / imax) * i)
            let jColour = Math.floor(255 - (255 / jmax) * j)
            ctx.fillStyle = composeRGBstring(iColour,jColour)
            ctx.fillRect(j, i, 1, 1)
        }
    }
}

//fuck it up
function drawNoise(x: number, y: number, radius: number) {
    //boiler plate beginning
    //boiler plate end
    let left = x - radius
    let right = left + radius * 2
    let top = y - radius
    let bottom = top + radius * 2;
    for (var i = top; i <= bottom; ++i) {
        for (var j = left; j <= right; ++j) {
            var dist = Math.pow(x - j, 2.0) + Math.pow(y - i, 2.0)
            if (dist <= Math.pow(radius, 2)) {
                if (noiseMatrix[i] !== undefined) {
                    if (noiseMatrix[i][j] !== undefined) {
                        if (noiseMatrix[i][j] < 30) {
                            noiseMatrix[i][j] += (30 * ((radius - Math.sqrt(dist)) / radius))
                        } else {
                            noiseMatrix[i][j] += (10 * ((radius - Math.sqrt(dist)) / radius))
                        }
                        let noise = noiseMatrix[i][j]
                        let iColour = Math.floor(255 - (255 / imax) * i) + (Math.random() * noise) - noise / 2
                        let jColour = Math.floor(255 - (255 / jmax) * j) + (Math.random() * noise) - noise / 2
                        ctx.fillStyle = composeRGBstring(iColour,jColour)
                        //ctx.fillStyle = `rgb(0,0,0)`
                        ctx.fillRect(j, i, 1, 1)
                    }
                }
            }
            if (i === bottom && j === right) {
                setTimeout(setNotDrawing, Math.max(30,Math.min(240,radius)))
            }
        }
    }
}

//bleh idk how to make this less ugly
function composeRGBstring(iColour, jColour): string {
    let colours = [iColour, jColour]
    colours.splice(randTriad,0,randColour)
    return `rgb(${colours[0]},${colours[1]},${colours[2]})`
    //return `rgb(${rColour},${gColour},${bColour})`
}

//draw droplet circle effect
function drawCircles(xpos: number, ypos: number, radius, offset: number = Math.floor(Math.random() * 360)) {
    if (mousedown) {
        ctx.strokeStyle = "#ffffff"
        ctx.beginPath()
        //let arcStartAngle = ((radius*4%360 + offset) * Math.PI) / 180
        //let arcEndAngle = (((radius*4%360)+20 + offset) * Math.PI) / 180
        ctx.arc(xpos, ypos, radius, 0, Math.PI * 2)
        ctx.stroke()
        if (radius < imax-24) {
            setTimeout(drawCircles, 200, xpos, ypos, radius + 24, offset)
        }
    }
}

// HANDLING MY SHIT (as is not typical for me)
canvas.addEventListener('mousedown', function (e) {
    if (e.button === 0) {
        e.preventDefault()
        if(!drawing) {
            xpos = e.offsetX
            ypos = e.offsetY
            mouseDownTimeStamp = e.timeStamp
            mousedown = true
            drawCircles(xpos, ypos, 10)
        } else {
            console.log("i'm still drawing the noise for you!! pls be patient with me uwu'''''''''")
        }
    }
});

canvas.addEventListener('mouseup', function (e) {
    if (e.button === 0) {
        mousedown = false
        if (mouseDownTimeStamp !== 0) {
            drawing = true
            let radius: number = Math.max(Math.min(Math.floor(((Math.floor(e.timeStamp) - Math.floor(mouseDownTimeStamp)) / 6)), imax), Math.floor((imax/10)*0.6))
            drawNoise(xpos, ypos, radius)
            xpos = 0
            ypos = 0
            mouseDownTimeStamp = 0
        }
    }
});
